import glob
import logging
import os
import sys

from git import Repo
import telegram.error as tg_error
from telegram import (ParseMode, MessageEntity, ChatAction,
                      ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (CallbackQueryHandler, CommandHandler, Filters,
                          MessageHandler, Updater, ConversationHandler)

from settings import token, start_msg, ChooseTheme, EmojiTheme


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

USERNAME, NAME, PHOTO, SITE, THEME, FONT = range(6)


def start(update, context):
    """Send starting message"""
    reply_keyboard = [['Crea']]
    update.message.reply_text(start_msg,  parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True))


def make(update, context):

    context.user_data['msg1'] = update.message.reply_text(
        "*Creazione del tuo badge*\n\n👤: 🕑\n🧍: 🕑\n🖼: ❌\n🔗: ❌\n🎨: ❌\n\n" +
        "`Inserisci il tuo username...`", parse_mode=ParseMode.MARKDOWN)

    return USERNAME


def username(update, context):

    context.user_data['username_user'] = update.message.text
    context.user_data['msg2'] = context.bot.edit_message_text(
        chat_id=context.user_data['msg1'].chat.id, message_id=context.user_data['msg1'].message_id, text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: 🕑\n🖼: ❌\n🔗: ❌\n🎨: ❌\n\n" +
        "`Inserisci il tuo nome...`", parse_mode=ParseMode.MARKDOWN)

    return NAME


def name(update, context):
    context.user_data['name_user'] = update.message.text
    context.user_data['msg3'] = context.bot.edit_message_text(
        chat_id=context.user_data['msg2'].chat.id, message_id=context.user_data['msg2'].message_id, text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: "+context.user_data['name_user']+"\n🖼: ❌\n🔗: ❌\n🎨: ❌\n\n" +
        "`Inviami la tua foto o link...`", parse_mode=ParseMode.MARKDOWN)

    return PHOTO


def photo(update, context):
    try:
        context.bot.getFile(
            update.message.photo[-1].file_id).download("file_tmp.png")
        context.user_data['photo_user'] = ("file_tmp.png")
    except:
        context.user_data['photo_user'] = update.message.text

    context.user_data['msg3'] = context.bot.edit_message_text(
        chat_id=context.user_data['msg2'].chat.id, message_id=context.user_data['msg2'].message_id, text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: "+context.user_data['name_user']+"\n🖼: ✅\n🔗: ❌\n🎨: ❌\n\n" +
        "`Inviami il link del tuo sito...`", parse_mode=ParseMode.MARKDOWN)

    return SITE


def site(update, context):
    reply_keyboard = [['Chiaro', 'Scuro']]
    context.user_data['site_user'] = update.message.text
    context.bot.delete_message(
        chat_id=context.user_data['msg2'].chat.id, message_id=context.user_data['msg2'].message_id)
    context.user_data['msg3'] = update.message.reply_text(text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: "+context.user_data['name_user']+"\n🖼: ✅\n🔗: ✅\n🎨: ❌\n\n" +
                                                          "`Seleziona il tema da usare...`",  parse_mode=ParseMode.MARKDOWN,
                                                          reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True))

    return THEME


def theme(update, context):
    os.chdir("assets/")
    fileFont = []
    for font in glob.glob("*.ttf"):
        fileFont.append(font)

    reply_keyboard = [fileFont]
    context.user_data['theme_user'] = update.message.text
    context.bot.delete_message(
        chat_id=context.user_data['msg3'].chat.id, message_id=context.user_data['msg3'].message_id)

    context.user_data['msg4'] = update.message.reply_text(text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: "+context.user_data['name_user']+"\n🖼: ✅\n🔗: ✅\n🎨: "+EmojiTheme[context.user_data['theme_user']]+"\n\n" +
                                                          "`Scegli il font...`",  parse_mode=ParseMode.MARKDOWN,
                                                          reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True))

    return FONT


def font(update, context):
    reply_keyboard = [['Crea']]
    context.user_data['font_user'] = update.message.text

    os.chdir("../")
    os.system("python3 badge/badge.py {0} {1} {2} {3} {4} {5}".format(
        context.user_data['username_user'], "'"+context.user_data['name_user']+"'", "'" + context.user_data['photo_user']+"'", "'" + context.user_data['site_user']+"'", ChooseTheme[context.user_data['theme_user']], "'"+"assets/"+context.user_data['font_user'])+"'")

    context.bot.delete_message(
        chat_id=context.user_data['msg4'].chat.id, message_id=context.user_data['msg4'].message_id)
    update.message.reply_text(text="*Creazione del tuo badge*\n\n👤: "+context.user_data['username_user']+"\n🧍: "+context.user_data['name_user']+"\n🖼: ✅\n🔗: ✅\n🎨: " + EmojiTheme[context.user_data['theme_user']] + "\n\n" +
                              "`Fatto ✅`",  parse_mode=ParseMode.MARKDOWN,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True))

    context.bot.sendChatAction(
        chat_id=update.message.chat_id, action=ChatAction.UPLOAD_PHOTO)

    context.bot.sendDocument(
        chat_id=update.message.chat_id, document=open('result.png', 'rb'))
    os.remove('result.png')
    try:
        os.remove('file_tmp.png')
    except:
        pass
    return ConversationHandler.END


def dona(update, context):
    update.message.reply_text(f"Codice sorgente: \n" +
                              "[Badge](https://github.com/Fast0n/badge)\n\n" +
                              "Bot Telegram: \n" +
                              "[Badge Bot](https://github.com/Fast0n/badge_bot)\n\n" +
                              "Sviluppato da: \n" +
                              "[Fast0n](https://github.com/Fast0n)\n\n" +
                              "🍺 Se sei soddisfatto offri una birra allo sviluppatore 🍺", parse_mode=ParseMode.MARKDOWN)


def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('A dopo')

    return ConversationHandler.END


def error(update, context):
    """Log errors caused by updates"""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Starts the bot"""
    print("Avvio Badge_Bot")

    # Check if REPO exist
    try:
        Repo.clone_from("https://gitlab.com/Fast0n/badge", "badge")
    except:
        pass

    # Check if assets exist
    try:
        os.mkdir("assets")
    except:
        pass

    # Check if font exist
    if len(os.listdir('assets/')) == 0:
        print("Directory %s is empty" % "assets")
        sys.exit()

    # Create the Updater
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex('^(Crea)$'), make)],

        states={

            USERNAME: [MessageHandler(Filters.text & (~Filters.regex('^(Crea)$') & (~Filters.regex('^(Stop)$'))), username)],
            NAME: [MessageHandler(Filters.text & (~Filters.regex('^(Crea)$') & (~Filters.regex('^(Stop)$'))), name)],
            PHOTO: [MessageHandler((Filters.entity(MessageEntity.URL) |
                                    Filters.entity(MessageEntity.TEXT_LINK)) | Filters.photo & (~Filters.regex('^(Crea)$') & (~Filters.regex('^(Stop)$'))), photo)],
            SITE: [MessageHandler((Filters.entity(MessageEntity.URL) |
                                   Filters.entity(MessageEntity.TEXT_LINK)) & (~Filters.regex('^(Crea)$') & (~Filters.regex('^(Stop)$'))), site)],
            THEME: [MessageHandler(Filters.text & (~Filters.regex('^(Crea)$') & (~Filters.regex('^(Stop)$'))), theme)],
            FONT: [MessageHandler(Filters.text &
                                  (~Filters.regex('^(Crea)$')), font)]



        },

        fallbacks=[MessageHandler(Filters.text & (
            Filters.regex('^(Stop)$')), cancel)]

    )

    # Register handlers
    dp.add_handler(conv_handler)
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("dona", dona))

    # Register error handler
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
