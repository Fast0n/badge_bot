# Your Bot token (get it from BotFather)
token = 'TOKEN'

# Message send when user 'start' the bot
start_msg = "Benvenuto su @badge\_Bot\n"\
            "crea e condividi il tuo badge sui social in modo semplice e veloce.\n"

# File with 'started' clients
client_file = "clients_id.txt"


ChooseTheme = {
    "Chiaro": "light",
    "Scuro": "dark"
}

EmojiTheme = {
    "Chiaro": "🌞",
    "Scuro": "🌙"
}
